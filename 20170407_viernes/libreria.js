function dimeUltimoDiaDelMes(fecha){
	f=new Date(fecha.getFullYear(), fecha.getMonth()+1, 0, 0 , 0, 0, 0);
	return f.getDate();
}

//Quiero crearme un un MES en forma de calendario
//la variable mes, sera el MES real Enero=1, Febrero=2
function pintaMes(mes, anyo){
	r='';
	r+='<table border="1">';
	r+='<tr>';
	r+='<td>L</td>';
	r+='<td>M</td>';
	r+='<td>X</td>';
	r+='<td>J</td>';
	r+='<td>V</td>';
	r+='<td>S</td>';
	r+='<td>D</td>';
	r+='</tr>';
	f=new Date(anyo, mes-1, 1, 0, 0, 0, 0);
	comienzoSemana=f.getDay(); //Me devuelve el numero de dia de la semana
	if(comienzoSemana==0){
		comienzoSemana=7;
	}
	//3-> miercoles
	if(comienzoSemana>1){
		r+='<tr>';
		for(i=0;i<comienzoSemana-1;i++){
			r+='<td>&nbsp;</td>';
		}
	}
	for(i=1;i<=dimeUltimoDiaDelMes(f);i++){
		fAux=new Date(anyo, mes-1, i, 0, 0, 0, 0);
		if(fAux.getDay()==1){
			r+='<tr>';
		}
		r+='<td>'+i+'</td>';
		if(fAux.getDay()==0){
			r+='</tr>';
		}
	}
	r+='</table>';
	return r;
}

function pintaAnyo(anyo, columnas=3){
	r='';
	r+='<table border="1">';
	for(m=1;m<=12;m++){
		if(m%columnas==1){
			r+='<tr>';
		}
		r+='<td>'+pintaMes(m,anyo)+'</td>';
		if(m%columnas==0){
			r+='</tr>';
		}
	}
	r+='</table>';
	return r;
}